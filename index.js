/* eslint-disable import/extensions */
import * as React from 'react';
import { useState } from 'react';

// constants and libs
import Translation from 'libs/Translation';

// components
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import MenuItem from '@material-ui/core/MenuItem';
import ReviewSection from '@/components/Dialog/reviewSection';
import TextInput from '@/components/Form/TextInput';
import CustomTypography from '@/components/Typography';
import CustomIconButton from '@/components/IconButton';
import Select from '@/components/Form/Select';
import FormControl from '@/components/Form/FormControl';
import { Box, Wrapper, AccordionDetail } from './style';
import CustomDatePicker from '@/components/DatePicker';

const buttonNext = {
	transform: 'scale(0.6)',
	marginRight: '0',
	border: '3px solid #3949ab',
	borderRadius: '10px',
	marginTop: '10px',
	padding: '10px',
};

const PushNotification = () => {
	const [expanded, setExpanded] = React.useState('panel1');
	const [titleValue, setTitleValue] = useState();
	const [textValue, setTextValue] = useState();
	const [urlValue, setUrlValue] = useState();
	const [nameValue, setNameValue] = useState();
	const [option, setOption] = useState();
	const [open, setOpen] = React.useState(false);
	const [date, setDate] = useState();

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleChange = panel => (_event, isExpanded) => {
		setExpanded(isExpanded ? panel : false);
		setExpanded(panel);
	};

	const options = [
		{ id: 1, label: 'Bookapo Production', value: 'Bookapo Production' },
		{ id: 2, label: 'Bookapo Debug', value: 'Bookapo Debug' },
		{ id: 3, label: 'Bookapo Staging', value: 'Bookapo Staging' },
		{ id: 4, label: 'Bookapo', value: 'Bookapo' },
		{ id: 5, label: 'Bookapo iOS', value: 'Bookapo iOS' },
	];

	const handleOptionChange = e => {
		setOption(e.target.value);
	};

	const handleTitleChange = e => {
		setTitleValue(e.target.value);
	};
	const handleTextChange = e => {
		setTextValue(e.target.value);
	};
	const handleUrlChange = e => {
		setUrlValue(e.target.value);
	};
	const handleNameChange = e => {
		setNameValue(e.target.value);
	};
	const handleDate = e => {
		setDate(e.target.value);
	};
	const handleSubmit = () => {
	};
	return (
		<Wrapper>
			<Accordion
				expanded={expanded === 'panel1'}
				onChange={handleChange('panel1')}
			>
				<AccordionSummary
					aria-controls="panel1bh-content"
					id="panel1bh-header"
				>
					<CustomTypography
						style={{ paddingRight: '20px' }}
					>
						{Translation.t('label.notif')}
					</CustomTypography>
				</AccordionSummary>
				<AccordionDetail>
					<FormControl>
						<TextInput
							className="input"
							placeholder={Translation.t('label.notif_title_placeholder')}
							value={titleValue}
							variant="outlined"
							label={Translation.t('label.notif_title')}
							onChange={handleTitleChange}
						/>
						<TextInput
							className="input"
							placeholder={Translation.t('label.notif_text_placeholder')}
							value={textValue}
							variant="outlined"
							label={Translation.t('label.notif_text')}
							onChange={handleTextChange}
						/>
						<TextInput
							className="input"
							placeholder={Translation.t('label.notif_img_placeholder')}
							value={urlValue}
							variant="outlined"
							label={Translation.t('label.notif_img')}
							onChange={handleUrlChange}
						/>
						<TextInput
							className="input"
							placeholder={Translation.t('label.notif_name_placeholder')}
							variant="outlined"
							label={Translation.t('label.notif_name')}
							onChange={handleNameChange}
							value={nameValue}
						/>
					</FormControl>
					<CustomIconButton
						className="button"
						onClick={handleChange('panel2')}
						style={buttonNext}
					>
						{Translation.t('label.next')}
					</CustomIconButton>
				</AccordionDetail>
			</Accordion>
			<Accordion
				expanded={expanded === 'panel2'}
				onChange={handleChange('panel2')}
			>
				<AccordionSummary
					aria-controls="panel2bh-content"
					id="panel2bh-header"
				>
					<CustomTypography
						style={{ paddingRight: '20px' }}
					>
						{Translation.t('label.notif_target')}
					</CustomTypography>
				</AccordionSummary>
				<AccordionDetails>
					<Box>
						<div>
							<CustomTypography
								style={{ marginBottom: '5px' }}
							>
								{Translation.t('label.notif_target_app')}
							</CustomTypography>
							<Select
								value={option}
								onChange={handleOptionChange}
								style={{ width: '200px' }}
							>
								{options.map(option => (
									<MenuItem
										key={option.id}
										value={option.value}
										style={{ padding: '5px', paddingRight: '10px', cursor: 'pointer' }}
									>
										{option.label}
									</MenuItem>
								))}
							</Select>
						</div>
						<div>
							<CustomTypography
								style={{ marginBottom: '5px' }}
							>
								{Translation.t('label.notif_target_user')}
							</CustomTypography>
							<Select style={{ width: '200px' }} />
						</div>
					</Box>
					<CustomIconButton
						style={buttonNext}
						onClick={handleChange('panel3')}
					>
						{Translation.t('label.next')}
					</CustomIconButton>
				</AccordionDetails>
			</Accordion>
			<Accordion
				expanded={expanded === 'panel3'}
				onChange={handleChange('panel3')}
			>
				<AccordionSummary
					aria-controls="panel3bh-content"
					id="panel3bh-header"
				>
					<CustomTypography style={{ paddingRight: '20px' }}>
						{Translation.t('label.notif_Scheduling')}
					</CustomTypography>
				</AccordionSummary>
				<AccordionDetails>
					<div style={{ margin: 'auto', width: '200px' }}>
						<CustomTypography style={{ marginBottom: '5px' }}>زمان ارسال</CustomTypography>
						<CustomDatePicker style={{ width: '100%' }} onChange={handleDate} value={date} />
					</div>
				</AccordionDetails>
			</Accordion>
			<div className="button">
				<CustomIconButton
					className="button-item"
					onClick={handleSubmit}
				>
					{Translation.t('label.send_button')}
				</CustomIconButton>
				<CustomIconButton
					className="button-item"
					onClick={handleClickOpen}
				>
					{Translation.t('label.review_button')}
				</CustomIconButton>
				<ReviewSection
					open={open}
					close={handleClose}
					titleValue={titleValue}
					textValue={textValue}
					option={option}
					date={date}
				/>
			</div>
		</Wrapper>
	);
};

/* Export ===================================== */
export default PushNotification;
