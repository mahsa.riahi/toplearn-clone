import _ from "lodash"

export const paginate =(courses , currenetPage , numCourses) =>{
     
    const startIndex = (currenetPage -1) * numCourses

    return _(courses).slice(startIndex).take(numCourses).value()
}