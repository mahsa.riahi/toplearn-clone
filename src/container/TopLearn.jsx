import React, { useEffect } from 'react';
import Layout from '../components/common/layout/Layout';
import Course from './../components/common/Course';
import {Route , Switch , Redirect} from "react-router-dom"
import Login from './../components/common/Login';
import Register from './../components/common/Register';
import Archive from './../components/common/Archive';
import SingleCourse from './../components/common/SingleCourse';
import {useSelector , useDispatch} from "react-redux"
import { paginate } from '../utils/paginate';
import { decodeToken } from './../utils/decodeToken';
import Logout from '../components/common/Logout';
import { addUser } from './../actions/user';
import {clearUser} from "./../actions/user"
import { isEmpty } from 'lodash';
import Profile from './../components/common/profile';
import UserContextapi from './../components/userContext/userContextapi';
import NotFound from './../components/common/NotFound';

const TopLearn = (props) => {


const dispatch = useDispatch()
const courses = useSelector(state => state.courses)
const user = useSelector(state => state.user)
const indexCourses = paginate(courses , 1 , 8)


useEffect(() =>{
  require("../utils/script")
  const token = localStorage.getItem("token")

  if(!isEmpty(token)) {
    const _decodeToken = decodeToken(token)
    const now = Date.now() /1000
    const exp = _decodeToken.payload.exp

    if(exp < now) {
      localStorage.removeItem("token")
      dispatch(clearUser())

    }

    else{
      dispatch(addUser(_decodeToken.payload.user))
    }

  }
},[])

    return ( 
  <Layout>
              <Switch>
                      <Route path="/login" render={() => isEmpty(user) ? <UserContextapi><Login/></UserContextapi> : <Redirect to="/" />}/>
                      <Route path="/logout" render={() => !isEmpty(user) ? <Logout /> : <Redirect to="/" /> }/>
                      <Route path="/register" render={() => isEmpty(user) ? <UserContextapi><Register/></UserContextapi> : <Redirect to="/" />}/>
                      <Route path="/user-profile" component={Profile} />
                      <Route path="/archive" component={Archive} />
                      <Route path="/course/:id" component={SingleCourse} />
                      <Route path="/" exact render={() => <Course courses={indexCourses} />} />
                      <Route path="*" exact component={NotFound} />
              </Switch>
 </Layout>  

     )
}
 
export default TopLearn;