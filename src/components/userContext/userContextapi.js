import React, { useState , useRef} from "react";
import {showLoading , hideLoading} from "react-redux-loading-bar"
import { useDispatch } from 'react-redux';
import {registerUser} from "./../../services/UserService"
import SimpleReactValidator from "simple-react-validator"
import { decodeToken } from './../../utils/decodeToken';
import { addUser } from './../../actions/user';
import { UserContext } from './UserContext';
import { toast } from 'react-toastify';
import {loginUser} from "./../../services/UserService"
import { withRouter } from 'react-router-dom';

const UserContextapi = (props) => {

    const dispatch = useDispatch()


    const [fullname, setFullname] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const[policy , setPolicy] = useState()
    const [ , forceUpdate] = useState()


    
    const resetRegister = () => {
        setFullname("");
        setEmail("");
        setPassword("");
    
    };

    const resetLogin=() =>{
        setEmail("")
        setPassword("")

    }

    const validator = useRef(new SimpleReactValidator({
        messages :{
            required : "این فیلد الزامی است.",
            min : "برای این فیلد حداقل 5 کاراکتر الزامی است.",
            email : "ایمیل نامعتبر است."
        },
        element : message => <div style={{color:"red"}}>{message}</div>

    }))

    const handleRegister= async event => {
        event.preventDefault();
        const userRegister = {
            fullname,
            email,
            password
        };

        try {
            if(validator.current.allValid()){
                dispatch(showLoading())
            const { status } = await registerUser(userRegister);
            if (status === 201) {
                toast.success("شما با موفقیت ثبت نام شدید.", {
                    position: "top-right",
                    closeOnClick: true
                });
     
                props.history.push("/login")
                resetRegister();
            }
        }else{
               dispatch(hideLoading())
                validator.current.showMessages()
                forceUpdate(1)

            }
          }  catch (ex) {
             dispatch(hideLoading())
            toast.error("مشکلی پیش آمده.", {
                position: "top-right",
                closeOnClick: true
               });
            
            console.log(ex);
        
    }}

    const handleLogin = async event =>{
        event.preventDefault()

        const userLogin ={
            email,
            password
        }
        
        try{
            if(validator.current.allValid()){
                dispatch(showLoading())
            const { status, data } = await loginUser(userLogin);
            if(status === 200){
                dispatch(hideLoading())
                toast.success("شما با موفقیت وارد شدید." , {
                    position:'top-right',
                    closeOnClick:true
                })
            localStorage.setItem("token" , data.token)
           dispatch(addUser(decodeToken(data.token).payload.user))
        
            props.history.replace("/")
            resetLogin()}
        }

        else{
            forceUpdate(1)
            validator.current.showMessages()
        }
    }
        catch(ex){
            dispatch(hideLoading())
            toast.error("مشکلی رخ داده است." , {
                position: "top-right",
                closeOnClick:true
            })

        }

    }

    

    return ( 

        <UserContext.Provider value={{
               fullname,
                setFullname,
                email,
                setEmail,
                password,
                setPassword,
                policy,
                setPolicy,
                validator,
                handleLogin,
                handleRegister
            }}
            >
               {props.children}
         </UserContext.Provider>
     
    )}

export default withRouter(UserContextapi);