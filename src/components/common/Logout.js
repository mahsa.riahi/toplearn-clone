import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { clearUser } from '../../actions/user';
import { withRouter } from 'react-router-dom';

const Logout = (props) => {

    const dispatch = useDispatch()

    useEffect(() =>{
        localStorage.removeItem("token")
        dispatch(clearUser())
        props.history.push("/")
    },[])

    return null
}

export default withRouter(Logout);
