import React, { Fragment } from 'react';
import MainNav from './../MainNav';
import Footer from './../Footer';
import Header from './../Header';
import TopNav from './../TopNav';
import {withRouter} from 'react-router-dom'
import { Helmet } from 'react-helmet';
import LoadingBar from "react-redux-loading-bar"


const Layout = (props) => {
    const {pathname} = props.location
    return (
        <Fragment>
            <Helmet>
                <title>خودآموز تاپلرن</title>
            </Helmet>

            <div className="landing-layer">
  
                <div className="container">
                <LoadingBar style={{ backgroundColor:"lime" , height:"5px"}} />
                    <TopNav />
                    {pathname === "/" ? <Header /> : null}
                </div>
            </div>

            <MainNav />

            <main id="home-page">
                <div className="container">{props.children} </div>
            </main>

            <Footer />
        </Fragment>
      );
}
 
export default withRouter(Layout);