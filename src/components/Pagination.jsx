import React from 'react';
import {range} from "lodash"

const Pagination = ({totalCourses , currentPage ,numCourses , onPageChange }) => {


   const numPages = Math.ceil(totalCourses / numCourses)
   console.log(numPages)

   if (numPages === 1) {
    return null
}

    const pages = range(1 , numPages + 1)
    
    console.log(pages)
    return ( 
        <nav aria-label="Page navigation">
        <ul className="pagination justify-content-center">
        
        {pages.map(page => (
            <li key={page} className={page===currentPage ?"page-item active" : "page-item"}>
                <a className="page-link" style={{cursor : "pointer"}} onClick={() => onPageChange(page)}>
                    {page}
                </a>
            </li>
        ))}
    
            
        </ul>
    </nav>

     );
}
 
export default Pagination;