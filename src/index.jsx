import React from 'react';
import {render} from "react-dom"
import {BrowserRouter} from "react-router-dom"
import TopLearn from './container/TopLearn';
import {ToastContainer} from "react-toastify"
import {Provider} from "react-redux"
import {store} from "../src/store/store"


render(<Provider store={store}><BrowserRouter><TopLearn /><ToastContainer/></BrowserRouter></Provider> , document.getElementById("root"))